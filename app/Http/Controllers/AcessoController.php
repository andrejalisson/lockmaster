<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AcessoController extends Controller{
    public function login(){
        $title = "Login";
        return view('admin.acesso.login')->with(compact( 'title'));
    }

    public function recupera(){
        $title = "Recuperar Senha";
        return view('admin.acesso.recupera')->with(compact( 'title'));
    }

    public function recuperaPost(Request $request){
        $usuario = DB::table('usuarios')->where('email', $request->email)->first();
        if($usuario != null){
            $request->session()->flash('sucesso', 'Um email foi enviado com as instruções!');
            return redirect('/');
        }else{
            $request->session()->flash('erro', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }

    public function verifica(Request $request){
        $usuario = DB::table('usuarios')->where('email', $request->email)->first();
        if($usuario != null){
            if($usuario->status_user != 0){
                if(password_verify($request->senha, $usuario->senha )){
                    $request->session()->flash('sucesso', 'Bom trabalho!');
                    $request->session()->put('id', $usuario->id);
                    $request->session()->put('nome', $usuario->nome);
                    $request->session()->put('imagem', $usuario->imagem);
                    $request->session()->put('usuario', $usuario->usuario);
                    $request->session()->put('logado', true);
                    return redirect('/Servicos');
                }else{
                    $request->session()->flash('erro', 'Senha incorreta!');
                    return redirect()->back();
                }
            }else{
                $request->session()->flash('erro', 'Usuário Bloqueado.');
            return redirect()->back();
            }
            
        }else{
            $request->session()->flash('erro', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }


}
