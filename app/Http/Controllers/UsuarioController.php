<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller{
    public function usuarios(){
        $title = "Usuários";
        return view('admin.usuario.lista')->with(compact( 'title'));
    }

    public function add(){
        $title = "Adicionar Usuário";
        return view('admin.usuario.add')->with(compact( 'title'));
    }

    public function edt($id){
        $title = "Editar Usuário";
        $usuario = DB::table('usuarios')->where('id_user', $id)->first();
        return view('admin.usuario.edt')->with(compact('title', 'usuario'));
    }

    public function edtPost(Request $request){
        if ($request->senha == "") {
            DB::table('usuarios')
                ->where('id_user', $request->id)
                ->update([
                    'nome_user' => $request->nome,
                    'usuario_user' => $request->usuario,
                    'imagem_user' => $request->imagem,
                    'email_user' => $request->email
                  ]);
        }else{
            DB::table('usuarios')
                ->where('id_user', $id)
                ->update([
                    'nome_user' => $request->nome,
                    'usuario_user' => $request->usuario,
                    'imagem_user' => $request->imagem,
                    'email_user' => $request->email,
                    'senha_user' => password_hash($request->senha, PASSWORD_DEFAULT)
                ]);
        }
        
        return "Salvo";
    }

    public function addPost(Request $request){
        DB::table('usuarios')->insert([
            'nome_user' => $request->nome,
            'usuario_user' => $request->usuario,
            'imagem_user' => $request->imagem,
            'email_user' => $request->email,
            'senha_user' => password_hash($request->senha, PASSWORD_DEFAULT),
            'status_user' => 1,
            ]
        );
        return "Salvo";
    }

    public function todosUsuarios(Request $request){
        $columns = array(
            0 =>'id_user',
            1 =>'nome_user',
            2 =>'usuario_user',
            3 =>'email_user',
            3 =>'status_user',
        );        
        $totalData = DB::table('usuarios')
                        ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $usuarios = DB::table('usuarios')
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }
        else{
            $search = $request->input('search.value');
            $usuarios =  DB::table('usuarios')
                            ->where('nome_user','LIKE',"%{$search}%")
                            ->orwhere('usuario_user','LIKE',"%{$search}%")
                            ->orwhere('email_user','LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            $totalFiltered = DB::table('usuarios')
                            ->count();
        }
        $data = array();
        if(!empty($usuarios)){
            foreach ($usuarios as $usuario){
                $nestedData['id'] = "# ".$usuario->id_user;
                $nestedData['imagem'] = "<img alt=\"image\" width=\"48\" height=\"48\" class=\"rounded-circle\" src=\"".$usuario->imagem_user."\">";
                $nestedData['nome'] = $usuario->nome_user;
                $nestedData['usuario'] = $usuario->usuario_user;
                $nestedData['email'] = $usuario->email_user;
                if ($usuario->status_user == 1) {
                    $nestedData['status'] = "<span class=\"label label-primary\">Ativo</span>";
                }else{
                    $nestedData['status'] = "<span class=\"label label-danger\">Inativo</span>";
                }
                $nestedData['opcoes'] = "   <a class=\"btn btn-warning btn-circle\" href=\"/EditarUsuario/".$usuario->id_user."\" type=\"button\"><i class=\"fa fa-pencil\"></i></a>
                                            <a class=\"btn btn-danger btn-circle\" href=\"/BloquearUsuario/".$usuario->id_user."\" type=\"button\"><i class=\"fa fa-ban\"></i></a>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }
}
