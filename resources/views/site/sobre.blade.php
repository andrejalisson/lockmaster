@extends('templates.site')

@section('css')
@endsection

@section('corpo')

<section class="about-style-two">
    <div class="container">
        <div class="row">
            {{-- <div class="col-lg-6">
                <div class="video-block-style-one">
                    <img src="images/resources/video-1-1.png" alt="Awesome Image" />
                </div><!-- /.video-block-style-one -->
            </div><!-- /.col-lg-6 --> --}}
            <div class="col-lg-12 d-flex">
                <div class="content-block my-auto">
                    <div class="title-block">
                        <span class="tag-line">Sobre nós</span><!-- /.tag-line -->
                        <h2>30 Anos de experiência</h2>
                    </div><!-- /.title-block -->
                    <p>A A.S Rastreamento pertence ao Grupo A.S, com profissionais experientes há mais de 30 anos. Utiliza os mais modernos sistemas e equipamentos de rastreamento e segurança veicular existentes no mercado.</p>
                    <a href="#" class="more-btn">Solicite seu Orçamento.</a>
                </div><!-- /.content-block -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-style-two -->
<section class="mission-style-one wow fadeInUp home-page-two" data-wow-duration="600ms">
    <div class="container">
        <div class="inner-container">
            <div class="single-mission-one">
                <div class="count-block">
                    01
                </div><!-- /.count-block -->
                <h3>Visão</h3>
                <p>Buscar em primeiro lugar a satisfação e segurança dos clientes e  oferecer um produto acessível a preço justo, integrando sempre qualidade e segurança.</p>
            </div><!-- /.single-mission-one -->
            <div class="single-mission-one">
                <div class="count-block">
                    02
                </div><!-- /.count-block -->
                <h3>Missão</h3>
                <p>Oferecer soluções em rastreamento inteligente, promovendo a otimização de tempo e redução de custo, focando o aumento da expectativa de lucro nas atividades dos nossos clientes.</p>
            </div><!-- /.single-mission-one -->
            <div class="single-mission-one">
                <div class="count-block">
                    03
                </div><!-- /.count-block -->
                <h3>Valores</h3>
                <p>A excelência e a qualidade do trabalho que exercemos. Através do  carinho pelo que fazemos, a lucratividade se torna uma consequência desses atos.</p>
            </div><!-- /.single-mission-one -->
        </div><!-- /.inner-container -->
    </div><!-- /.container -->
</section><!-- /.mission-style-one -->

@endsection

@section('js')
@endsection

@section('script')
@endsection
