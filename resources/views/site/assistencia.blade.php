@extends('templates.site')

@section('css')
@endsection

@section('corpo')

<section class="about-style-one">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex">
                <div class="content-block my-auto">
                    <div class="title-block">
                        <span class="tag-line">Atendimento 0800 24h</span><!-- /.tag-line -->
                        <h2>Fique zen e deixe<br> tudo <strong>com a gente</strong></h2>
                    </div><!-- /.title-block -->
                    <p>Nós temos o melhor atendimento 0800 para sua empresa. Assistência 24/7 para seus veículos em parceria com a <a href="http://www.numclickassistencias.com.br/" target="blank">NumClick Assistências.</a> </p>
                    <a target="blank" href="https://wa.me/558597943530" class="more-btn">Solicite agora</a>
                </div><!-- /.content-block -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="image-block">
                    <img src="/site/images/resources/story-1-1.png" alt="Awesome Image" />
                </div><!-- /.image-block -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-style-one -->
<section class="about-style-one">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="image-block">
                    <img src="/site/images/resources/story-1-2.png" alt="Awesome Image" />
                </div><!-- /.image-block -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6 d-flex">
                <div class="content-block my-auto">
                    <div class="title-block">
                        <span class="tag-line">FIQUE TRANQUILO COM UM SERVIÇO DE ATENDIMENTO 0800 24/7</span><!-- /.tag-line -->
                        <h2>O melhor custo benefício do mercado.</h2>
                    </div><!-- /.title-block -->
                    <p>Nossa central de atendimento 0800 tem uma equipe de atendentes 24/7 para atender melhor lhe atender.</p>
                    <a target="blank" href="https://wa.me/558597943530" class="more-btn">Solicite agora</a>
                </div><!-- /.content-block -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-style-one -->

<section class="contact-info-style-one">
    <div class="container">
        <div class="title-block text-center">
            <span class="tag-line">ASSISTÊNCIA 24 HORAS <a href="http://www.numclickassistencias.com.br/" target="blank">NUMCLICK ASSISTÊNCIA.</a></span><!-- /.tag-line -->
            <h2>Cobertura em todo território Brasileiro 24/7</h2>
        </div><!-- /.title-block -->
        <div class="row high-gutter">
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>
                        <h3>Reboque 100km</h3>
                        <p>Com até 2 remoções por ano, o Serviço de Guincho cobre até 100km em todo Brasil.</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>
                        <h3>Aux. Mecânico</h3>
                        <p>Com o serviço de auxílio mecânico o cliente não fica na mão! Em caso de pane mecânica ou elétrica será enviado um profissional para ajudar em um reparo emergencial no local.</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Chaveiro</h3>
                        <p>Tenha ajuda em caso de perda da chave, esquecimento no interior do veículo e problemas com a fechadura.</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Troca de Pneus</h3>
                        <p>Em caso de dano no pneu do veículo, será enviado um profissional para troca pelo estepe ou reboque até uma borracharia que possa consertá-lo.</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Pane Seca</h3>
                        <p>A Lock Master te ajuda até mesmo na falta de combustível! Conte com um reboque para levar o veículo até um posto de serviço mais próximo.</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Acompanhamento Online</h3>
                        <p>Acompanhe via SMS o tempo estimado da chegada do resgate.</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Território Brasileiro</h3>
                        <p>Atendimento em todo o território Brasileiro com 0800 24 horas.</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3">
                <div class="single-contact-info-one text-center">
                    <div class="inner-block">
                        <i class="cameron-icon-placeholder"></i>

                        <h3>Acionamentos</h3>
                        <p>Vigência de 12 meses / 1 acionamento por mês / 2 por serviço no Ano.</p>
                    </div><!-- /.inner-block -->
                </div><!-- /.single-contact-info-one -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.contact-info-style-one -->

@endsection

@section('js')
@endsection

@section('script')
@endsection
