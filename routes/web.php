<?php

use Illuminate\Support\Facades\Route;

//Site
Route::get('/', 'SiteController@home')->name('home');
// Route::get('/Sobre', 'SiteController@sobre')->name('sobre');
Route::get('/Planos', 'SiteController@planos')->name('planos');
Route::get('/Assistencia24h', 'SiteController@assistencia')->name('assistencia');
Route::get('/Contato', 'SiteController@contato')->name('contato');
Route::post('/EnviarMensagem', 'SiteController@mensagemPost')->name('enviarmensagem');
Route::get('/Newsletter', 'SiteController@newsletterPost')->name('newsletterPost');
//Acesso
Route::get('/Login', 'AcessoController@login')->name('login');
Route::get('/Recupera', 'AcessoController@recupera')->name('recupera');
Route::post('/Login', 'AcessoController@verifica');
Route::post('/Recupera', 'AcessoController@recuperaPost');
//Usuários
Route::get('/Usuarios', 'UsuarioController@usuarios');
Route::post('/todosUsuarios', 'UsuarioController@todosUsuarios')->name('todosUsuarios');
Route::get('/AdicionarUsuario', 'UsuarioController@add');
Route::post('/AdicionarUsuario', 'UsuarioController@addPost');
Route::get('/EditarUsuario/{id}', 'UsuarioController@edt');
Route::post('/EditarUsuario', 'UsuarioController@edtPost');
//Serviços
Route::get('/Servicos', 'ServicoController@servico');
Route::post('/todosServicos', 'ServicoController@todosServicos')->name('todosServicos');
Route::post('/todosServicosLixeira', 'ServicoController@todosServicosLixeira')->name('todosServicosLixeira');
Route::get('/AdicionarServicos', 'ServicoController@add');
Route::get('/EditarServicos/{id}', 'ServicoController@edt');
Route::get('/ExcluirServicos/{id}', 'ServicoController@excluirPost');
Route::post('/EditarServicos/{id}', 'ServicoController@edtPost');
Route::get('/ServicosLixeira', 'ServicoController@lixo');
Route::post('/AdicionarServicos', 'ServicoController@adicionarServicos');